#include "string.h"

/**
* Method to reverse the string given in the 'str' parameter
* and store the result in the 'result' parameter.
**/
char* reverse(char *str) {
    int length = strlen(str) - 1;
    int middle = length / 2;

    // iterate through the input string from the back to the 
    // front, while incrementing the result pointer the other
    // way.
    
    for (int i = 0; i <= middle; i++) {
        char temp = str[i];
        str[i] = str[length - i];
        str[length - i] = temp;
    }

    return str;
}