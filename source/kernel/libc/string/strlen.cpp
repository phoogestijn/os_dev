#include "string.h"

/** 
 * Method to retrieve the length of a string. The method
 * wil count all characters, until a '\0' character is found.
 **/         
int strlen(const char *str) {
    int length = 0;
    while (*str++ != 0) {
        length++;
    }

    return length;
}