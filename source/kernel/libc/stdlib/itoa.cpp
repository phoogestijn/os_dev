#include "stdlib.h"
#include "string.h"

/**
 * Convert a integer value to a ASCII string. The result will be
 * stored in the given 'str' parameter. 
 **/
char* itoa(int value, char *str, int base)
{
    // Initialization
    int i = 0;
    bool negative = false;

    // in case we got a negative number, with base = 10
    // we add a '-' character to the result. For all other
    // values of base, we assume the value to be unsigned.
    if (value < 0 && base == 10) 
    {
        negative = true;
        value = -value;
    }

    // because we transformed any negative number in to a positive
    // number a few lines above, value can only be 0 or larger 
    // than 0. 
    if (value > 0) 
    {
        while (value > 0) 
        {
            // Get the remainder after dividing the value by the
            // given base. This is the number that will be translated
            // into a digit.
            char digit = value % base;

            // Update the value for further use.
            value /= base;

            // Translate the digit into a readable ASCII character.
            str[i++] = (digit < 10) ? digit + '0' : (digit - 10) + 'A';
       }
    } 
    else 
    {
        // In the case value = 0, we only have to print a zero character.
        str[i++] = '0';
    }

    // add the minus sign at the end, so it will be reverted to 
    // the front of the string.
    if (negative) 
    {
        str[i++] = '-';
    }

    // When base 16 is given, we will present the value as a HEX number.
    // Therefore we add the '0x'.
    if (base == 16) 
    {
        str[i++] = 'x';
        str[i++] = '0';
    }

    // Last, we have to terminate the character stream by adding a 
    // null-terminator. 
    str[i++] = '\0';

    // Because we have processed the value from the least significant 
    // to most significant digit, we have to reverse the entire character
    // stream.
    reverse(str);

    // Return the reversed stream.
    return str;
}


