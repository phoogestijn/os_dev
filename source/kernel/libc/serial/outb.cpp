#include "serial.h"

/**
 * Method to write the specified char value, to the processors
 * I/O-port specified. 
 **/
void outb(unsigned short port, unsigned char value)
{
    asm volatile ("outb %0, %1" :: "a" (value), "dN" (port));

    
    // a --> put value in register eax
// dN --> put port in register edx or use 1-byte 

}