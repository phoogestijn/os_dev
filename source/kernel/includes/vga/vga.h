#ifndef VGA_H
#define VGA_H

/**
The VGA class contains a set of method needed for interacting
vith the screen of the computer. 
*/
class VGA 
{

	public:
		/**
		 * Default constructor. Used to initialize an instance of the 
		 * VGA class. 
		 **/
		VGA();

		/**
		 * Method for printing text to the video memory. Placeholders 
		 * can be used to insert the variables passed as arguments
		 * to this method.
		 */
		void printf(const char *str, ...);
		
		/**
		 * Method for setting the fore- and background color that must 
		 * be used to write to video memory. Both the fore- and background
		 * colors can be changed between each line that is written to 
		 * video memory.
		 */
		void set_color(short foreground, short background);

	private: 
		// We store the video memory address here as a constant, so
		// that we don't have to remember the exact location in our
		// implementation.
		static const int VGA_MEM = 0xB8000;

		// we also store the width and height of the screen, for the
		// same reason. In the standard VGA mode, we can use a screen
		// of 80 character in with and 25 lines in height.
		static const int VGA_HEIGHT = 25;
		static const int VGA_WIDHT = 80;

		// we need to store the X and Y location of the cursor on the 
		// screen, so we can write text at the cursor position.
		static int _cursor_x;
		static int _cursor_y;

		// private field to store both the fore- and background color in
		// a 32 bit integer.
		int _color;

		/**
		 * Method to write a string of characters to the video buffer.
		 */
		void puts(char* string);

		/**
		 * Method to write a single character to the video buffer.
		 */
		void putchar(char val);

		/** 
		 * Moves the cursor onto the starting position of the next line
		 * on the screen.
		 */
		void new_line(void);
};

/*
Enumeration of the hardware colors that can be used in 32 bit 
protected kernel mode.
*/
enum Colors {
	VGA_COLOR_BLACK = 0,
	VGA_COLOR_BLUE = 1,
	VGA_COLOR_GREEN = 2,
	VGA_COLOR_CYAN = 3,
	VGA_COLOR_RED = 4,
	VGA_COLOR_MAGENTA = 5,
	VGA_COLOR_BROWN = 6,
	VGA_COLOR_LIGHT_GREY = 7,
	VGA_COLOR_DARK_GREY = 8,
	VGA_COLOR_LIGHT_BLUE = 9,
	VGA_COLOR_LIGHT_GREEN = 10,
	VGA_COLOR_LIGHT_CYAN = 11,
	VGA_COLOR_LIGHT_RED = 12,
	VGA_COLOR_LIGHT_MAGENTA = 13,
	VGA_COLOR_LIGHT_BROWN = 14,
	VGA_COLOR_WHITE = 15,
};

#endif