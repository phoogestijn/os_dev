#ifndef STDIO_H
#define STDIO_H

#ifdef __cplusplus
extern "C" {
#endif

int printf(const char *str, ...);

#ifdef __cplusplus
}
#endif

#endif