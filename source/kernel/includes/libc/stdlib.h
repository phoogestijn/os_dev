#ifndef STDLIB_H
#define STDLIB_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Convert a integer value to a ASCII string. The result will be
 * stored in the given 'str' parameter. 
 * 
 * @param value     The value to convert to an ASCII string
 * @param str       Pointer to the result string
 * @param base      The base value that should be used for
 *                  translation
 **/
char* itoa(int value, char* str, int base);



#ifdef __cplusplus
}
#endif

#endif
