#ifndef COMMON_H
#define COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

/**
* Method to reverse the string given in the 'str' parameter
* and store the result in the 'result' parameter.
* 
* @param: str      The string that must be reverted
* @param: result   The pointer to te result string
* @return          The pointer to te result string
**/
char* reverse(char *str);

/** 
* Method to retrieve the length of a string. The method
* wil count all characters, until a '\0' character is found.
* 
* @param str      The string for which the length will be determinded
* @return         The length of the string excluding the terminator
**/         
int strlen(const char *str);



#ifdef __cplusplus
}
#endif

#endif

