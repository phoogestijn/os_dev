#ifndef SERIAL_H
#define SERIAL_H

#ifdef __cplusplus
extern "C" {
#endif

void outb(unsigned short port, unsigned char value);

#ifdef __cplusplus
}
#endif

#endif