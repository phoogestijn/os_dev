#include "types.h"
#include "vga.h"
#include "stdlib.h"
#include "serial.h"
#include "string.h"

struct kernel_version_info {
    unsigned short major;
    unsigned short minor;
    unsigned short patch;
};

extern "C" int handle_add(int a, int b);
extern "C" int log_message(int i);

void main()
{
    VGA video;
    video.printf("Starting the kernel on memory address: %x\n", (int) main);
    // video.printf("Kernel version: %d.%d.%d\n", version_info.major, version_info.minor, version_info.patch);
    video.printf("There is still a lot to do, but printing this entire message\n");
    video.printf("is one big step!\n");
	
    int a = 15, b = 20;
    int result = handle_add(a, b);

    video.printf("\n\nResult: %d", result);
    

    // reboot
    // outb(0x64, 0xFE);

    //while(1){};
    // return 1;
}

int log_message(int i)
{
    VGA video;
    video.printf("LOG MESSAGE: %d\n", i);

    return i + 10;
}