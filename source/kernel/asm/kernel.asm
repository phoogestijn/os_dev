global handle_add

[bits 32]
[extern log_message]

handle_add:
    mov     eax, [esp+4]    ; read the first argument of the stack
    add     eax, [esp+8]    ; read the second argument of the stack

    push    ebp             ; push current stack frame
    mov     ebp, esp        ; store current stack point to current stack frame

    push    eax             ; push result of sum to stack, as first argument of function

    call    log_message     ; call function
    
    leave                   ; reset stackpointers
    ret                     ; return to c code.