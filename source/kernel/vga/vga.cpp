#include "vga.h"
#include "stdlib.h"
#include "types.h"
#include <stdarg.h>

// Set the initial cursor position at the left top corner
// of the screen.
int VGA::_cursor_x = 0;
int VGA::_cursor_y = 0;

/** 
 * Implementation of the default constructor. This method sets the
 * initial cursor values.
 **/
VGA::VGA(void)
{
    // Set the default colors
    set_color(VGA_COLOR_WHITE, VGA_COLOR_BLACK);
}

void VGA::putchar(char val) 
{
    // retrieve the video memory address and increment it with
    // the given x and y coordinates.
    volatile uint16_t *video = (volatile uint16_t*) 0xB8000 + (_cursor_y * 80 + _cursor_x++);

    // If we have reached the end of the screen, we jump to the 
    // next line.
    if (_cursor_x == VGA_WIDHT)
    {
        new_line();
    }

    // If the current character is the new-line characer.
    switch(val)
    {
        case '\n':
            new_line();
            break;

        case '\r':
            break;
        
        default:
            *video = val | (_color << 8);
    }
}

void VGA::puts(char* string)
{
    while (*string != '\0')
    {
        putchar(*string++);
    }
}

void VGA::printf(const char* format, ...)
{
    va_list args;
    va_start(args, format);

    while (*format != '\0')
    {
        if (*format != '%')
        {
            putchar(*format);
            format++;
        }
        else
        {
            format++;
            switch (*format)
            {
                case 'c':
                {
                    char i = (char) va_arg(args, int);
                    putchar(i);
                }
                break;
                
                case 's':
                {
                    char *str = va_arg(args, char*);
                    puts(str);
                }
                break;

                case 'd':
                {
                    int i = va_arg(args, int);
                    char *buffer[12];
                    puts(itoa(i, *buffer, 10));
                }
                break;
                
                case 'o':
                {
                    int i = va_arg(args, int);
                    char *buffer[12];
                    puts(itoa(i, *buffer, 8));
                }
                break;
                    
                case 'x':
                {
                    int i = va_arg(args, int);
                    char *buffer[12];
                    puts(itoa(i, *buffer, 16));
                }
                break;
            }
            format++;
        }
    }
}

void VGA::new_line(void)
{
    _cursor_x = 0;
    _cursor_y++;
}

void VGA::set_color(short foreground, short background )
{
    _color =  foreground | (background << 4);
}