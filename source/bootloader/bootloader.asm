; ------------------------------------------------------------------------------------
; bootloader.asm
;
; Bootleader to load a kernel into memory and start executing it. There is no exception
; handing or user feedback implemented. When things go wrong, the machine will just crash.
;
; Author			: Pieter Hoogestijn
; Date				: 2017-01-12
; ------------------------------------------------------------------------------------

; Tell the assembler that all addresses are relative to
; address 0x7C00. This is the address used by the BIOS 
; to load the bootloader to.
[ORG 0x7c00]

; We start in 16bit mode. Later, this will be switched to
; 32bit mode.
[bits 16]
	; We define the kernel version, so we can pass it to 
	; the main function of the kernel. There is no real
	; need to do it this way. It is just because we can!
	KERNEL_MAJOR 	equ 0x00
	KERNEL_MINOR 	equ 0x00
	KERNEL_PATCH 	equ 0x01

	; Define the memory location we will load the kernel to
	; Make sure, this value is the same as the one used in
	; the link process of the kernel.
    KERNEL_OFFSET 	equ 0x1000	; We load the kernel here
	KERNEL_SIZE 	equ 0x15	; Number of sectors to read
	BOOT_DRIVE 		equ 0x00	; 1st floppy is boot drive

	; Fill the segment values (stored in the DS
	; register), to zero. This can be zero, because we already
	; told the assembler that our origin is 0x7c00
	xor		ax, ax				; Clear AX
	mov 	ds, ax				; Move AX to the DS 
	
	; We now will start loading the kernel onto the predefined
	; memory location. First of all, we reset the disk drive.
	;mov ah, 0x00				; Set 'Reset disk drive' option 
	;mov dl, BOOT_DRIVE			; We want to reset the bootdisk
	;int 0x13					; execute the action
	
	; Now the diskdrive is being reset, we can start reading from
	; disk drive into memory.
	mov ah, 0x02				; Instruct processor to read data
	mov bx, KERNEL_OFFSET		; into the predefined memory location
	mov al, KERNEL_SIZE			; Set how much sectors must be read
	mov ch, 0x00				; cylinder 	= 0
	mov dh, 0x00				; head		= 0
	mov dl, BOOT_DRIVE			; we read from the bootdrive
	mov cl, 0x02				; start reading from the second sector
								; because the bootloader is in the first.
	int 0x13					; read data into memory
	   
    ; When the kernel is loaded, nothing is in our way to switch
	; to protected mode. After switching to protected mode, the
	; systems runs in 32bit mode. Because the most interupts are
	; not supported anymore in protected mode, we first disable
	; the interupts.
    cli
	
	; Before we switch to protected mode, the A20 line had to be enabled.
	; The A20 Address Line is the physical representation of the 21st bit 
	; (number 20, counting from 0) of any memory access. For an operating 
	; system developer (or Bootloader developer) this means the A20 line 
	; has to be enabled so that all memory can be accessed. In three lines
	; below, use the "FAST A20" option, supported by newer computers.
	in al, 0x92
	or al, 2
	out 0x92, al

	; In protected mode, the segment register becomes an index pointing
	; to a segment register. which is located in the Global Descriptor
	; Table (GDT). The GDT separates the available memory space into
	; segments with their own purpose (CODE, DATA, etc). On the next
	; line, we load the GDT defined at the bottom this file.
    lgdt [gdt_descriptor]
    
	; Once the GDT is being loaded. We can gracefully jump into protected
	; mode. The one thing we have to do, is setting the first bit of the
	; cr0 CPU control register to 1. Since we cannot write directly into
	; the cr0 register, we copy the value into eax, switch the first bit
	; and copy the new value back into cr0.
    mov eax, cr0                
    or  eax, 0x01
    mov cr0, eax
	
	; After setting the first cr0 bit, the processor will be in protected
	; mode. To force the processor to finish all pending 16bit operations
	; in the "pipeline", we jmp to a memory address in another segment.
	; Since all segments are 8 bytes long and we want to jump to the second
	; segment, the segment offset will be 0x08.
    jmp 0x08:load_kernel
    
; After the far-jump to force the processor switch its processing mode to,
; 32bit, we will endup at the label defined below. 
load_kernel:
[bits 32]

	; After the jump, also the segment registeres must point to a
	; GDT segment index. Because we want to point to the DATA segment,
	; we have to skip the first two segments. The segment offset will
	; therefor be 2 x 8 = 16 bytes (= 0x10) na het begin van de GDT.
    mov ax, 0x10
    mov ds, ax
    mov ss, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    
	; To make sure the A20 line is succesfully set, we perform a little
	; test. If is succeeds, we continue loading the kernel. Otherwise, 
	; we skip loading the kernel and stop the bootloader.
	pushad
	mov edi,0x112345  ;	Set an odd megabyte address.
	mov esi,0x012345  ; Set an even megabyte address.
	
	; We now make sure that both addresses contain a different value. If 
	; the A20 line is cleared, the two pointers whould both point to the
	; address 0x012345. 
	mov [esi],esi     
	mov [edi],edi     			  
	cmpsd
	popad

	; If the addresss are not the same, the A20 line is set correctly and
	; we continue loading the kernel. If both addressses are the same, the
	; A20 lines is cleared and we stop the bootloader, before loading the 
	; kernel.
	jne start_kernel        
	ret               

start_kernel:

	; Before we can call the kernel. We need to set the stackpointer, add
	; the end of the available space. 
    mov ebp, 0x90000
    mov esp, ebp
	
	; We push the method arguments, in reverse order onto the call stack
	push word KERNEL_PATCH
	push word KERNEL_MINOR 
	push word KERNEL_MAJOR

    ; The next line will make the processor to jump to the memory
	; address we stored the kernel on, so it will start executing
	; the kernel. If everything is OK, we will not return from the
	; next call. 
    call KERNEL_OFFSET
    
	; In case we do comeback from ther kernel, we start een infinite
	; loop, by jumping to the current instruction.
	jmp	$			

; ------------------------------------------------------------------------------------

; Below, the global descriptor table (GDT) is defined. In the GDT,
; segments are defined separating the available memory space into
; segments with a specific purpose.

; We are stil in 16bits mode.
[bits 16]

; In the GDT defined below, three segments descriptors are defined
; of 8 bytes each. The first descriptor is the required null (empty)
; segment. After that, we define 2 overlapping segments; one for code
; and one for data.
GDT:

; The null segment consists of 8 bytes, with all 0's.
   dd 0x00
   dd 0x00

; The next 8 bytes describe the "code" segment. The limit is set to
; the maxmimum value, the segment base is 0 (start at the beginning).

   dw 0xffff        ; byte 00 .. 15 of the LIMIT value
   dw 0x0000        ; byte 00 .. 15 of the BASE value
   db 0x0000        ; byte 16 .. 23 of the BASE value
   db 10011010b     ; 1st FlAGS: (present)1 (privilege)00 (descriptor type)1 			--> 1001
					; type flag: (code)1 (conforming)0 (readable)1 (accessed)0 			--> 1010
					
   db 11001111b     ; 2nd FLAGS: (granularity)1 (32-bit default)1 (64-bit seg)0 (AVL)0 	--> 1100
					; byte 16 .. 19 of the LIMIT										--> 1111
					
   db 0x0000        ; byte 23 .. 31 of the BASE

; The next 8 bytes describe the "data" segment. The limit is set to
; the maxmimum value, the segment base is 0 (start at the beginning).
; This practically means that both the data and code segment overlap
; each other. The only difference between the two segments, is the
; value of the type flag: code.

   dw 0xffff        ; byte 00 .. 15 of the LIMIT value
   dw 0x0000        ; byte 00 .. 15 of the BASE value
   db 0x0000        ; byte 16 .. 23 of the BASE value
   db 10010010b     ; 1st FlAGS: (present)1 (privilege)00 (descriptor type)1 			--> 1001
					; type flag: (code)0 (conforming)0 (readable)1 (accessed)0 			--> 1010
					
   db 11001111b     ; 2nd FLAGS: (granularity)1 (32-bit default)1 (64-bit seg)0 (AVL)0 	--> 1100
					; byte 16 .. 19 of the LIMIT										--> 1111
					
   db 0x0000        ; byte 23 .. 31 of the BASE

; The next lines describe the reference bytes that point to the GDT. This
; reference contains 2 values: 
;  - The total size of the GDT - 1
;  - The start address of the GDT in memory.
gdt_descriptor :
   dw $ - GDT - 1    ; The 16-bits containing the size of the GDT (minus 1)
   dd GDT            ; The 32-bit address pointing to the start of the GDT
   
; The bootloader must be 512 bytes, and the last two bytes
; must have the values 0x55 and 0xAA.
times 510-($-$$) db 0
db 0x55
db 0xAA